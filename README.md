# Estrutura dos Testes

O projeto foi estruturado da seguinte forma:

Package Runner (br.ce.hs.runners): Onde a execução do projeto é chamada.

Package Steps (br.ce.hs.steps): Onde o teste foi implementado.

Folder features: Onde os cenários dos testes estão sendo descritos.

# Execução dos Testes

Para que seja possível executar os testes, será necessário alterar o caminho do ChromeDriver, o arquivo está integrado junto ao projeto, para isso basta alterar no "System.setProperty", para o caminho onde o projeto foi clonado.

Para a execução basta acessar o projeto no eclipse e executar a classe RunnerTest.java.