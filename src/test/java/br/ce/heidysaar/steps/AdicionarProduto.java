package br.ce.heidysaar.steps;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.es.Dado;
import cucumber.api.java.it.Quando;

import cucumber.api.java.pt.Ent�o;

public class AdicionarProduto {
	private WebDriver driver;
	

	@Dado("^que eu acesse a pagina$")
	public void que_eu_acesse_a_pagina() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C://Users//heidy.oliveira//Documents//H2H//H2H//chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://williams-sonoma.com");	
	}

	@Quando("^acesso a �rea do item$")
	public void acesso_a_�rea_do_item() throws Throwable {
		driver.findElement(By.xpath("//*[@id=\"topnav-container\"]/ul/li[1]/a")).click();		
		driver.findElement(By.xpath("//*[@id=\"super-category\"]/main/div[2]/div[1]/ul[1]/li[1]/a")).click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("img.product-thumb.hoverSwap")));
		driver.findElement(By.cssSelector("img.product-thumb.hoverSwap")).click();
			}

	@Quando("^adiciono um item no carrinho realizando seu Checkout$")
	public void adiciono_um_item_no_carrinho_realizando_seu_Checkout() throws Throwable {
		driver.findElement(By.id("primaryGroup_addToCart_0")).click();
		driver.findElement(By.id("anchor-btn-checkout")).click();
	}

	@Ent�o("^o produto deve estar na p�gina do carrinho de compras$")
	public void o_produto_deve_estar_na_p�gina_do_carrinho_de_compras() throws Throwable {
		assertEquals("All-Clad d5 Stainless-Steel 10-Piece Cookware Set", driver.findElement(By.linkText("All-Clad d5 Stainless-Steel 10-Piece Cookware Set")).getText());
	}

	@Quando("^eu pesquiso por um item$")
	public void eu_pesquiso_por_um_item() throws Throwable {
		driver.findElement(By.id("search-field")).sendKeys("fry pan");
	    driver.findElement(By.id("btnSearch")).click();
	}

	@Quando("^clico em QuickLook$")
	public void clico_em_QuickLook() throws Throwable {
		driver.findElement(By.cssSelector("span.quicklook-link")).click();
	}

	@Ent�o("^a exibi��o r�pida ser� apresentada$")
	public void a_exibi��o_r�pida_ser�_apresentada() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("overlay-content")));
		driver.findElement(By.id("overlay-content"));
	}
}
