#language: pt
Funcionalidade: Adicionar ao carrinho
 
Contexto: 
 	Dado que eu acesse a pagina 
	
Cenário: Adicionar produto ao carrinho	
	Quando acesso a área do item
	E adiciono um item no carrinho realizando seu Checkout
	Então o produto deve estar na página do carrinho de compras
	
Cenário: Ver um produto no quick look overlay  
  Quando eu pesquiso por um item
  E clico em QuickLook
	Então a exibição rápida será apresentada