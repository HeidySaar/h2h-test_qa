$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/AdicionarProduto.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language: pt"
    }
  ],
  "line": 2,
  "name": "Adicionar ao carrinho",
  "description": "",
  "id": "adicionar-ao-carrinho",
  "keyword": "Funcionalidade"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Contexto"
});
formatter.step({
  "line": 5,
  "name": "que eu acesse a pagina",
  "keyword": "Dado "
});
formatter.match({
  "location": "AdicionarProduto.que_eu_acesse_a_pagina()"
});
formatter.result({
  "duration": 38312355501,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Adicionar produto ao carrinho",
  "description": "",
  "id": "adicionar-ao-carrinho;adicionar-produto-ao-carrinho",
  "type": "scenario",
  "keyword": "Cenário"
});
formatter.step({
  "line": 8,
  "name": "acesso a área do item",
  "keyword": "Quando "
});
formatter.step({
  "line": 9,
  "name": "adiciono um item no carrinho realizando seu Checkout",
  "keyword": "E "
});
formatter.step({
  "line": 10,
  "name": "o produto deve estar na página do carrinho de compras",
  "keyword": "Então "
});
formatter.match({
  "location": "AdicionarProduto.acesso_a_área_do_item()"
});
formatter.result({
  "duration": 3286242812,
  "error_message": "org.openqa.selenium.WebDriverException: unknown error: Element \u003ca href\u003d\"https://www.williams-sonoma.com/shop/cookware/cookware-sets/?cm_type\u003dlnav\"\u003e...\u003c/a\u003e is not clickable at point (111, 539). Other element would receive the click: \u003cdiv class\u003d\"stickyOverlayScroll overlayScroll\" style\u003d\"overflow: auto scroll;\"\u003e\u003c/div\u003e\n  (Session info: chrome\u003d69.0.3497.100)\n  (Driver info: chromedriver\u003d2.41.578737 (49da6702b16031c40d63e5618de03a32ff6c197e),platform\u003dWindows NT 6.1.7601 SP1 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 0 milliseconds\nBuild info: version: \u00273.14.0\u0027, revision: \u0027aacccce0\u0027, time: \u00272018-08-02T20:19:58.91Z\u0027\nSystem info: host: \u0027DGDBHZ002\u0027, ip: \u0027169.254.159.29\u0027, os.name: \u0027Windows 7\u0027, os.arch: \u0027amd64\u0027, os.version: \u00276.1\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, acceptSslCerts: false, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 2.41.578737 (49da6702b16031..., userDataDir: C:\\Users\\HEIDY~1.OLI\\AppDat...}, cssSelectorsEnabled: true, databaseEnabled: false, goog:chromeOptions: {debuggerAddress: localhost:61388}, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: XP, platformName: XP, rotatable: false, setWindowRect: true, takesHeapSnapshot: true, takesScreenshot: true, unexpectedAlertBehaviour: , unhandledPromptBehavior: , version: 69.0.3497.100, webStorageEnabled: true}\nSession ID: 5683c6514bd2253db8bac58dc7398915\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)\r\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:80)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:44)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:548)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:276)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.click(RemoteWebElement.java:83)\r\n\tat br.ce.heidysaar.steps.AdicionarProduto.acesso_a_área_do_item(AdicionarProduto.java:31)\r\n\tat ✽.Quando acesso a área do item(src/test/resources/features/AdicionarProduto.feature:8)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "AdicionarProduto.adiciono_um_item_no_carrinho_realizando_seu_Checkout()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "AdicionarProduto.o_produto_deve_estar_na_página_do_carrinho_de_compras()"
});
formatter.result({
  "status": "skipped"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Contexto"
});
formatter.step({
  "line": 5,
  "name": "que eu acesse a pagina",
  "keyword": "Dado "
});
formatter.match({
  "location": "AdicionarProduto.que_eu_acesse_a_pagina()"
});
formatter.result({
  "duration": 12813094131,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Ver um produto no quick look overlay",
  "description": "",
  "id": "adicionar-ao-carrinho;ver-um-produto-no-quick-look-overlay",
  "type": "scenario",
  "keyword": "Cenário"
});
formatter.step({
  "line": 13,
  "name": "eu pesquiso por um item",
  "keyword": "Quando "
});
formatter.step({
  "line": 14,
  "name": "clico em QuickLook",
  "keyword": "E "
});
formatter.step({
  "line": 15,
  "name": "a exibição rápida será apresentada",
  "keyword": "Então "
});
formatter.match({
  "location": "AdicionarProduto.eu_pesquiso_por_um_item()"
});
formatter.result({
  "duration": 319119490,
  "status": "passed"
});
formatter.match({
  "location": "AdicionarProduto.clico_em_QuickLook()"
});
